#!/bin/bash

# SPDX-FileCopyrightText: 2019 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# *****************************************************************************
# * TITLE:        easyExtensions.sh                                           *
# * EXECUTION:    ./easyExtensions.sh                                         *
# * VERSION:      0.11                                                        *
# * LAST UPDATE:  2019-11-20                                                  *
# * CREATOR:      Damian Berghof, virengos.com                                *
# * EMAIL:        info@virengos.com                                           *
# * TESTED ON:    Ubuntu 18.04, ParaView 5.6.0                                *
# *****************************************************************************
# * Disclaimer:                                                               *
# * This offering is not approved or endorsed by OpenCFD Limited,             *
# * producer and distributor of the OpenFOAM software via www.openfoam.com,   *
# * and owner of the OPENFOAM® and OpenCFD® trade marks.                      *
# *****************************************************************************

'''
This bash script creates mesh extensions for inlet and outlet 
for an existing OF mesh
Detailed documentation for the extrudeMesh command can be found here:
https://bit.ly/2VHYwwV
'''

clear
echo "*** virengos OF mesh extender ***"
echo "================================="

echo "*** convert the current mesh into VTK format ***"
rm -rvf VTK-origin
foamToVTK
mv VTK VTK-origin

echo "*** original mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** backup current mesh ***"
mkdir -p constant/polyMesh_backup
cp -r constant/polyMesh/* constant/polyMesh_backup/
touch constant/polyMesh_backup/case.foam

echo "*** backup current extrude-config file ***"
cp system/extrudeMeshDict system/extrudeMeshDict.bak
sleep 1

echo "*** prepare new extrude-config file for inlet extension ***"
cp system/extrudeMeshDict_inlet system/extrudeMeshDict
sleep 1

echo "*** extrude inlet ***"
extrudeMesh
echo "*** 1st extruded mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** prepare new extrude-config file for outlet extension ***"
cp system/extrudeMeshDict_outlet system/extrudeMeshDict
sleep 1

echo "*** extrude outlet ***"
extrudeMesh
echo "*** 2nd extruded mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** restore original extrude-config file ***"
cp system/extrudeMeshDict.bak system/extrudeMeshDict

echo "*** mesh patches have been extended ***"
echo "*** and can be reviewed in ParaView by opening the case.foam file! ***"

exit

# optional commands
foamToVTK
