# SPDX-FileCopyrightText: 2019 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# postProcessing Folder
postFolder = 'postProcessing/cuttingPlane/'
# variables (must be defined in the controlDict or somewhere else)
postVariables = ['U', 'p']
