#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2019 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# *****************************************************************************
# * TITLE:        easyPostCopyPlanes.py                                       *
# * EXECUTION:    ./python3.6 easyPostCopyPlanes.py                           *
# * VERSION:      0.02                                                        *
# * LAST UPDATE:  2019-11-20                                                  *
# * CREATOR:      Damian Berghof, virengos.com                                *
# * EMAIL:        info@virengos.com                                           *
# * TESTED ON:    Ubuntu 18.04                                                *
# *****************************************************************************
# * Disclaimer:                                                               *
# * This offering is not approved or endorsed by OpenCFD Limited,             *
# * producer and distributor of the OpenFOAM software via www.openfoam.com,   *
# * and owner of the OPENFOAM® and OpenCFD® trade marks.                      *
# *****************************************************************************

'''
(1) this Python script copies all cutting planes with the VTK format into
a specific postProcessing folder
(2) file structure must have this syntax: <file_description1234.vtk> 
to be grouped when open in ParaView
(3) run outside of OpenFOAM docker container, due to missing Python libraries
'''

import os
import subprocess
from config import *

# check if an OF project folder exists
if any([os.path.exists('system') is False,
        os.path.exists('constant') is False,
        os.path.exists('postProcessing') is False]):
    raise IOError('*** Error: not an OpenFOAM simulation folder or missing '
                  'postProcessing folder***')
else:
    print('*** Info: OpenFOAM folder exists, continue ***')

# create additional folders for postProcessing
try:
    for var in postVariables:
        if not os.path.exists(postFolder + '/' + var):
            os.makedirs(postFolder + var)
            print('*** folder {} has been created ***'.format(postFolder + var))
except IOError:
    raise IOError('*** Error: could not create additional folder ***')

# reconstruct simulation results by startung external bash script
try:
    command = ["./easyReconstruct.sh"]
    print(command)
    output, error = subprocess.Popen(
        command, universal_newlines=True, shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()
except OSError:
    raise IOError('*** Error: could not start the <easyReconstruct.sh> '
                  '\script to execute OF reconstructPar ***')

# rename VTK files (cuttingPlanes)
for subdir, dirs, files in os.walk(postFolder):
    if subdir:
        if len(dirs) < 1:  # skip the main folder
            for file in files:
                if all([not file.startswith('file'),
                        file[0].isdigit() is False]):
                    stepNumber = subdir.split('/')[-1]
                    currentFile = (os.getcwd() + '/' +
                                   postFolder + str(subdir.split('/')[-1]) +
                                   '/' + file)
                    fileExtension = '.' + str(file.split('.')[1])
                    fileMod = file.split('.')[0]
                    targetFolder = os.getcwd() + '/' + postFolder + \
                                   stepNumber + '/'
                    newFile = (targetFolder + 'file_' + fileMod + '_' +
                               stepNumber + fileExtension)
                    os.rename(currentFile, newFile)

# find and copy files to new created postProcessing folders
try:
    for var in postVariables:
            proc_out = subprocess.check_output(['find', postFolder, '-name',
                                                '*_' + var + '_*.vtk',
                                                '-type', 'f', '-exec', 'cp',
                                                '{}',
                                                './' + postFolder + var, ';'])
except IOError:
    raise IOError('*** ERROR: could not find and copy VTK files ***')
