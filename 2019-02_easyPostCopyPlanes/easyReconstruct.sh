#!/bin/bash

# SPDX-FileCopyrightText: 2019 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# *****************************************************************************
# * TITLE:        easyReconstruct.sh                                          *
# * EXECUTION:    ./easyReconstruct.sh                                        *
# * VERSION:      0.01                                                        *
# * LAST UPDATE:  2019-11-20                                                  *
# * CREATOR:      Damian Berghof, virengos.com                                *
# * EMAIL:        info@virengos.com                                           *
# * TESTED ON:    Ubuntu 18.04                                                *
# *****************************************************************************
# * Disclaimer:                                                               *
# * This offering is not approved or endorsed by OpenCFD Limited,             *
# * producer and distributor of the OpenFOAM software via www.openfoam.com,   *
# * and owner of the OPENFOAM® and OpenCFD® trade marks.                      *
# *****************************************************************************

echo "*** INFO: reconstruct sub-script has been started! ***"
reconstructPar -newTimes
exit

