# Easy-Post-Runner
This is an ***experimental*** release of the Easy-Post-Runner

This Python script iterates over ParaView OpenFOAM PVSM templates and creates
screenshots for given time steps in the /visualization folder

###### Disclaimer
* This script should be used for testing only and is not recommended in production.
* This offering is not approved or endorsed by OpenCFD Limited,
producer and distributor of the OpenFOAM software via www.openfoam.com,
and owner of the OPENFOAM® and OpenCFD® trade marks.   

## Project structure (simplified)

```
├── 150
│   ├── p
│   └── U
├── 200
│   ├── p
│   └── U
├── case.foam
├── config.py
├── config.pyc
├── constant
│   ├── polyMesh
├── easyPostRunner.py
├── iso1.pvsm
├── iso1.pvsm.template
├── meshcheck-XY.pvsm
├── meshcheck-XY.pvsm.template
├── README.md
├── slice-XY.pvsm
├── slice-XY.pvsm.template
├── system
│   ├── controlDict
│   ├── fvSchemes
│   └── fvSolution
└── visualization
    ├── 150_iso1.png
    ├── 150_meshcheck-XY.png
    ├── 150_slice-XY.png
    ├── 200_iso1.png
    ├── 200_meshcheck-XY.png
    ├── 200_slice-XY.png
    ├── iso1.pvsm.template
    ├── meshcheck-XY.pvsm.template
    └── slice-XY.pvsm.template
```

## Getting Started

##### STEP 1
Open a terminal and download the repository to your home folder:

```
wget -cv https://gitlab.com/virengos/openfoam_public/-/archive/master/openfoam_public-master.tar.gz
```
You could also download the relevant scripts / files and include it into your project.

Run a first test with the delivered test data and defined min and max time steps
```
$ pvbatch --force-offscreen-rendering easyPostRunner.py 200 350
```
##### STEP 2
Copy your simulation data to this folder or add the script and the config file to any other OF simulation folder.

##### STEP 3
Create the PVSM templates in the ParaView GUI and save it as _.pvsm.template_ in the main OF folder. You will need to add the _.template_ extensions manually

##### STEP 4
Modify the _.pvsm.template_ files manually as follows:
* replace the time step, e.g. 200 with the variable $priv_time

##### STEP 5
Configure templates, pic names, paths, pic resolution and other parameters in the _config.py_ file

Example
```
path_template="./"
path_vis="/visualization/"
image_resX=980
image_resY=530

temp_png_dict={
"meshcheck-XY.pvsm.template":"meshcheck-XY.png",
"iso1.pvsm.template":"iso1.png",
"slice-XY.pvsm.template":"slice-XY.png"
}
```
##### STEP 6
Run the script
```
$ pvbatch --force-offscreen-rendering easyPostRunner.py <min> <max>
```

## Troubleshooting
* Check your MESA version in terminal with:

```
$ glxinfo | grep 'OpenGL version'
```
----

## About
### Maintainers
* Damian Berghof, Germany
* Email - (info - at - virengos - dot - com)
* Project website - [virengos.org](https://www.virengos.org)
* Mastodon / Fosstodon - virengos

### Contributors
* -

### Project Support & Funds
If you love FOSS and appreciate what we are working on, here might be a good and easy possibility to support me and my team!
* [PayPal](https://virengos.com/showcase/) - On our virengos.com web page
* [Patreon](https://www.patreon.com/virengos)
* Digital Ocean Droplet - [Digital Ocean Droplet](https://m.do.co/c/4dea95afbe60). That's the place I'm running my scalable OpenFOAM simulations and other experiments in the cloud.

### License
* See folder [LICENCES](https://gitlab.com/virengos/openfoam_public/tree/master/LICENSES) in the main repository
* See [ParaView License](https://www.paraview.org/paraview-license/)
* [Other Licences](https://spdx.org/licenses/)
