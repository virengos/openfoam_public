# SPDX-FileCopyrightText: 2019 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

path_template="./"
path_vis="/visualization/"
image_resX=980
image_resY=530

temp_png_dict={
"meshcheck-XY.pvsm.template":"meshcheck-XY.png",
"iso1.pvsm.template":"iso1.png",
"slice-XY.pvsm.template":"slice-XY.png"
}
