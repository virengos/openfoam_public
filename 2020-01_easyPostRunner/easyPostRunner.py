#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2020 Damian Berghof <info@virengos.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# *****************************************************************************
# * TITLE:        easyPostRunner.py                                           *
# * EXECUTION:    pvbatch --force-offscreen-rendering easyPostRunner.py \     *
# *               min_step max_step                                           *
# * VERSION:      0.1.0                                                       *
# * LAST UPDATE:  2020-01-12                                                  *
# * CREATOR:      Damian Berghof, virengos.com                                *
# * EMAIL:        info@virengos.com                                           *
# * TESTED ON:    GNU/Linux Ubuntu 18.04.3 LTS (Bionic Beaver)                *
# *               paraview version 5.6.0, Python 2.7.17
# *****************************************************************************
# * Disclaimer:                                                               *
# * This offering is not approved or endorsed by OpenCFD Limited,             *
# * producer and distributor of the OpenFOAM software via www.openfoam.com,   *
# * and owner of the OPENFOAM® and OpenCFD® trade marks.                      *
# *****************************************************************************

'''
(1) This script iterates over ParaView OpenFOAM *.pvsm templates 
    and creates screenshots for given time steps in the /visualization folder

(2) Copy your simulation data to this folder 
    or add the script and the config file to any other OF simulation folder

(3) Create the *.pvsm templates in the ParaView GUI
    and save it as *.pvsm.template in the main OF folder

(4) Modify the *.pvsm.template files manually as follows:
    - replace the time step, e.g. 200 with the variable $priv_time

(5) Configure templates, pic names, paths, pic resolution 
    and other parameters in the config.py file

(6) Check your MESA version in terminal with:
    $ glxinfo | grep 'OpenGL version'
'''

import os
import sys
from config import *
from paraview.simple import *

path_curr = os.getcwd()
path_curr_base = path_curr.split("/")[-1]
path_curr_parent = path_curr.split("/")[-2]
main_dir_list = os.walk('.').next()[1]
try:
    sys.argv[1] == True
    sys.argv[2] == True
    min_step = int(sys.argv[1])
    max_step = int(sys.argv[2])
except IOError:
    print('*** Error:  missing command arguments min /max ! ***')

def change_time_step(filename_in,keyword,ftime_step):
    '''
    It changes keywords in XML templates
    '''
    try:
        with open(filename_in, 'r') as file :
            print('*** (fun) Open template: {}'.format(filename_in))
            print('*** (fun) Modify this keyword: {}'.format(keyword))
            print('*** (fun) New keyword is: {} {}'.\
                  format(ftime_step,type(ftime_step)))
            filedata = file.read()
            filedata = filedata.replace(keyword, '"'+ftime_step+'"')
        filename_out = filename_in.split('.')[0]+'.'+filename_in.split('.')[1]
        print('*** (fun) Modified template is being saved as: {}'.\
              format(filename_out))
        with open(filename_out, 'w') as file:
            file.write(filedata)
    except IOError:
        print('*** Error: {} template can not be modified! ***'\
              .format(filename_in))
    return filename_out

# check if an OF project folder exists
if any([os.path.exists('system') is False,
        os.path.exists('constant') is False]):
    raise IOError('*** Error: not an OpenFOAM simulation folder ***')
else:
    print('*** Info: OpenFOAM folder exists, continue ***')

# Iterate over time folder
for folder in main_dir_list:
    if folder.isdigit():
        if all([int(folder) >= min_step, int(folder) <= max_step]):
            print("*** Time step folder found: {}".format(int(folder)))
            time_step = folder

            for key, value in temp_png_dict.items():
                template = key
                png_name = value
                file_png = path_curr+path_vis+folder+'_'+png_name
                print("*** Current Template: {} ***".format(template))
                print("*** Screenshot is being stored in: {}".format(file_png))
                template_load = change_time_step(template,"$priv_time",\
                                                 str(time_step))
                #print('### load this: {}'.format(template_load))
                servermanager.Connect()
                try:
                    servermanager.LoadState(template_load)
                except IOError:
                    print('*** Error: Servermanager cannot load file {}'.\
                          format(template))
                SetActiveView(GetRenderView())
            
                SaveScreenshot(file_png,
                            ImageResolution=[image_resX,image_resY],
                            FontScaling='Scale fonts proportionally',
                            OverrideColorPalette='',
                            StereoMode='No change',
                            TransparentBackground=0,
                            CompressionLevel='5')
                servermanager.Disconnect()
                # servermanager.ResetSession()