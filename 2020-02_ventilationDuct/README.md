Last Update: 12-01-2019

__Disclaimer:__ \
This offering is not approved or endorsed by OpenCFD Limited, producer and distributor of the OpenFOAM software via www.openfoam.com, and owner of the OPENFOAM®  and OpenCFD®  trade marks.

----

![pic1](Images/ventilation-duct.gif)
Ventilation duct visualized in ParaView

## About
### Maintainers
* Email - (info - at - virengos - dot - com)
* Website - [virengos.com](https://www.virengos.com)
* Newsletter - https://virengos.com/newsletter/
* GrabCAD - [virengos](https://grabcad.com/virengos-1)
* Mastodon - virengos
* Youtube - [virengos channel](https://www.youtube.com/channel/UCYwF43s_ksqIIw8dB7O_TFg?view_as=subscriber)
  * ParaView | Basics of Data Visualization | Best Practice
* Facebook Group - [ParaView CAE](https://www.facebook.com/groups/352772705405413/)
* Facebook Group - [Salome CAE](https://www.facebook.com/groups/625990377880716/)
* Facebook Group - [Open Source Engineering Tools](https://www.facebook.com/groups/Open.Source.Engineering.Tools/)

### Contributors
* -

----

## Recommended Links

* Digital Ocean Droplet - [Digital Ocean Droplet](https://m.do.co/c/4dea95afbe60)
 * That's the place I'm running my scalable OpenFOAM simulations in the cloud.
