#!/bin/bash
# title         :easyExtensions.sh
# description   :creates mesh extensions for inlet and outlet for an OF mesh
# author        :Damian Berghof (aka virengos)
# date          :2019-09-28
# version       :0.1
# usage         :easyExtensions.sh
# notes         :...
#==============================================================================

clear
echo "*** virengos OF mesh extender ***"
echo "================================="

echo "*** original mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** backup current mesh ***"
mkdir -p constant/polyMesh_backup
cp -r constant/polyMesh/* constant/polyMesh_backup/

echo "*** backup current extrude-config file ***"
cp system/extrudeMeshDict system/extrudeMeshDict.bak
sleep 1

echo "*** prepare new extrude-config file for inlet extension ***"
cp system/extrudeMeshDict_inlet system/extrudeMeshDict
sleep 1

echo "*** extrude inlet ***"
extrudeMesh
echo "*** 1st extruded mesh size ***"
checkMesh -noTopology | grep -m1 cells





echo "*** prepare new extrude-config file for outlet extension ***"
cp system/extrudeMeshDict_outlet_a system/extrudeMeshDict
sleep 1

echo "*** extrude outlet ***"
extrudeMesh
echo "*** 2nd extruded mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** restore original extrude-config file ***"
cp system/extrudeMeshDict.bak system/extrudeMeshDict




echo "*** prepare new extrude-config file for outlet extension ***"
cp system/extrudeMeshDict_outlet_b system/extrudeMeshDict
sleep 1

echo "*** extrude outlet ***"
extrudeMesh
echo "*** 2nd extruded mesh size ***"
checkMesh -noTopology | grep -m1 cells

echo "*** restore original extrude-config file ***"
cp system/extrudeMeshDict.bak system/extrudeMeshDict



exit
