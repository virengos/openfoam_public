#!/bin/bash

#sample -latestTime

#simpleFoam -postProcess -func turbulenceIntensity
#simpleFoam -postProcess -func ..

#simpleFoam -postProcess -func yPlus
#simpleFoam -postProcess -func totalPressureIncompressible
#simpleFoam -postProcess -func wallShearStress
#simpleFoam -postProcess -func wallGradU
#simpleFoam -postProcess -func vorticity

#postProcess -func 'patchAverage(name=obj_inlet,p)' > Log_BC
#postProcess -func 'patchAverage(name=obj_outlet,p)' >> Log_BC
#postProcess -func 'patchAverage(name=WALL,nut)' >> Log_BC
#postProcess -func 'patchAverage(name=P1,k)' >> Log_BC
#postProcess -func 'patchAverage(name=A,k)' >> Log_BC
#postProcess -func 'patchAverage(name=A,omega)' >> Log_BC
#postProcess -func 'patchAverage(name=WALL,omega)' >> Log_BC

#  -parallel         run in parallel
#  -time <ranges>    comma-separated time ranges - eg, ':10,20,40:70,1000:'

# mpirun -np 4 simpleFoam -parallel -postProcess -func wallShearStress -time '500:700'

exit

