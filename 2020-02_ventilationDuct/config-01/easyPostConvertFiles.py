#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
# Script-Name:  easyPostConvert.py
# Start:        python3.6 easyPostConvert.py
# Description:  this script converts OpenFOAM postprocessing data from *.dat
#               into *.csv files to make it readable in paraview or other 
#               software. It calculates also additional data
# Project:      OpenFOAM Automatization
# Version:      0.01
# Author:       Damian Berghof
# Email: -
# Licence:      GPLv3
# Date:         2019-10-09
# Python V.:    ...
# Tested on:    OS-Version, etc.
# Remarks:      run outside of OpenFOAM docker container, due to missing 
#               python libraries
#------------------------------------------------------------------------------

import pandas as pd
import os
import subprocess

# *** OP10 ***
residual_file = '/residuals.dat'
residual_file_mod = '/residuals.csv'
# define new header for the residuals file
headerNewRes = ['Time','p','Ux','Uy','Uz','k','omega']

# *** OP20 ***
patchIn = 'flowRatePatch(name=obj_inlet)'
patchOut = 'flowRatePatch(name=obj_outlet)'
# define new header
headerNewMFB_in = ['Time','oilflowInlet[m3/s]']
# define new header
headerNewMFB_out = ['Time','oilflowOutlet[m3/s]']
fileOutputMFB = '/oilflowBalance.csv'
surfaceRegion_name = '/surfaceFieldValue.dat' # '/surfaceRegion.dat'

#------------------------------------------------------------------------------
# OP 10 - convert residuals from *.dat -> *.csv
#------------------------------------------------------------------------------
# get last time folder
p = subprocess.Popen(['find','postProcessing/residuals/','-type',
                      'd'],stdout=subprocess.PIPE)
out = (p.communicate()[0].strip()).decode('utf-8')
lastTimeStep = out.split('\n')[-1].split('/')[-1]

# open the original *.dat file
fileRes_origin = 'postProcessing/residuals/'+lastTimeStep+residual_file

# define output filename
fileOut = os.path.dirname(fileRes_origin)+residual_file_mod

# import data into pandas
dfRes = pd.read_table('postProcessing/residuals/'+lastTimeStep+residual_file,
                     sep='\s+',skiprows=2,header=None,index_col=0,
                     names=headerNewRes)

# export cleaned data to CSV
dfRes.to_csv(fileOut,sep=' ',index=True)

print('*** residual file {} has been converted! ***'.format(fileOut))

#------------------------------------------------------------------------------
# OP 20 - convert flow rate @ patch -> l/min, combine both to one file
#------------------------------------------------------------------------------
p = subprocess.Popen(['find','postProcessing/'+patchIn+'/',
                      '-type','d'],stdout=subprocess.PIPE)
out = (p.communicate()[0].strip()).decode('utf-8')
lastTimeStep = out.split('\n')[-1].split('/')[-1]  

# open the original *.dat file
file = ('postProcessing/'+patchIn+'/'+lastTimeStep+surfaceRegion_name)

# import data into pandas
dfInlet = pd.read_table('postProcessing/'+patchIn+'/'+
                     lastTimeStep+surfaceRegion_name,
                     sep='\s+',skiprows=4,header=None,index_col=0,
                     names=headerNewMFB_in)

#------------------------
# import Outlet data
# open the original *.dat file
file = ('postProcessing/'+patchOut+'/'+lastTimeStep+surfaceRegion_name)

# import data into pandas
dfOutlet = pd.read_table('postProcessing/'+patchOut+'/'+
                     lastTimeStep+surfaceRegion_name,
                     sep='\s+',skiprows=4,header=None,index_col=0,
                     names=headerNewMFB_out)

# ----------------------

# calculate additional columns
#oilflowInlet = -1*(dfInlet[dfInlet.columns[0]]/float(855))*60000.0
oilflowInlet = -1*(dfInlet[dfInlet.columns[0]]/float(1))*60000.0

# calculate additional columns
#oilflowOutlet = -1*(dfOutlet[dfOutlet.columns[0]]/float(855))*60000.0
oilflowOutlet = -1*(dfOutlet[dfOutlet.columns[0]]/float(1))*60000.0

dfOilflow = pd.DataFrame()

dfOilflow['oilflowInlet[lpm]'] = oilflowInlet
dfOilflow['oilflowOutlet[lpm]'] = oilflowOutlet

# define output filename
fileOutput = os.path.dirname(file)+fileOutputMFB

# export cleaned data to CSV
dfOilflow.to_csv(fileOutput,sep=' ',index=True)

print('*** oilflow outlet file {} has been converted! ***'.format(file))
