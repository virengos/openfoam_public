#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Script-Name:  easyPostCopyPlanes.py
# Start:        python3.6 easyPostCopyPlanes.py
# Description:  copy all OpenFOAM cutting planes in vtk format into
#               a postProcessing folder
# Project:      OpenFOAM postProcessing Automation
# Version:      0.02
# Author:       Damian Berghof, virengos.com
# Email: -
# Date:         2019-11-05
# Python V.:    -
# Tested on:    -
# Remarks:      Numbers must have this syntax: file_description1234.vtk
#               to be grouped when open in ParaView
# Remarks:      run outside of OpenFOAM docker container, due to missing 
#               python libraries
# ------------------------------------------------------------------------------
import os
import subprocess
from config import *

# check if an OpenFOAM project folder exists
if any([os.path.exists('system') is False,
        os.path.exists('constant') is False,
        os.path.exists('postProcessing') is False]):
    raise IOError('*** Error: not an OpenFOAM simulation folder or missing '
                  'postProcessing folder***')
else:
    print('*** Info: OpenFOAM folder exists, continue ***')

# create additional folders for postProcessing
try:
    for var in postVariables:
        if not os.path.exists(postFolder + '/' + var):
            os.makedirs(postFolder + var)
            print('*** folder {} has been created ***'.format(postFolder + var))
except IOError:
    raise IOError('*** Error: could not create additional folder ***')

# reconstruct simulation results
# start external bash script
try:
    command = ["./easyReconstruct.sh"]
    print(command)
    output, error = subprocess.Popen(
        command, universal_newlines=True, shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()
except OSError:
    raise IOError('*** Error: could not start the <easyReconstruct.sh> '
                  '\script to execute OF reconstructPar ***')

# rename VTK files (cuttingPlanes)
for subdir, dirs, files in os.walk(postFolder):
    if subdir:
        if len(dirs) < 1:  # skip the main folder
            for file in files:
                if all([not file.startswith('file'),
                        file[0].isdigit() is False]):
                    stepNumber = subdir.split('/')[-1]
                    currentFile = (os.getcwd() + '/' +
                                   postFolder + str(subdir.split('/')[-1]) +
                                   '/' + file)
                    fileExtension = '.' + str(file.split('.')[1])
                    fileMod = file.split('.')[0]
                    targetFolder = os.getcwd() + '/' + postFolder + \
                                   stepNumber + '/'
                    newFile = (targetFolder + 'file_' + fileMod + '_' +
                               stepNumber + fileExtension)
                    os.rename(currentFile, newFile)

# find and copy files to new created postProcessing folders
# TODO outsource the find and copy process to a bash file
try:
    for var in postVariables:
            proc_out = subprocess.check_output(['find', postFolder, '-name',
                                                '*_' + var + '_*.vtk',
                                                '-type', 'f', '-exec', 'cp',
                                                '{}',
                                                './' + postFolder + var, ';'])
except IOError:
    raise IOError('*** ERROR: could not find and copy VTK files ***')