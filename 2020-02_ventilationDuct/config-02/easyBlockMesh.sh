#!/bin/bash 
#------------------------------------------------------------------------------
# Script-Name:  easyBlockMesh
# Start:	./easyBlockMesh.sh <refinementFactor 0..max>
# Project:      ...
# Description:  xxx
# Version:      0.01
# Author:       xxx Damian Berghof
# Email: -
# Licence:      GNU GPLv3 (based on the OpenFOAM CFD toolbox, www.openfoam.org)
# Subject:  	this script creates automated blockMeshDict based on 
#           	all STL files placed in the constant/triSurface directory.
#           	Expected object dimensions in [mm]
# Date:         2019-05-22
# Python V.:    -
# Tested on:    -
# Remarks:
#------------------------------------------------------------------------------




# hier noch zweiter Übergabeparameter zum Teilen der Zellen !
# checke im Netz was es neues zu diesem Thema gibt !


clear

# <000> clean current working directory
    rm -f log*
    rm -f Log*
    rm -f *.obj
    mv VTK VTK.bak
    rm -fr VTK-*
    mv system/blockMeshDict system/blockMeshDict.bak

# <100> variables
    # for final blockMesh
    scaleFactorMesh=1.2
    # define the created file
    file=system/blockMeshDict

# Source tutorial run functions
#. $WM_PROJECT_DIR/bin/tools/RunFunctions

echo ""
echo "*---------------*"
echo "* easyBlockMesh *"
echo "*---------------*"

# <150> merge all STL objects
cat constant/triSurface/obj*.stl > constant/triSurface/allObjects.stl
echo "<150> merge all STL objects"

# <200> calculate bounding box 
surfaceCheck constant/triSurface/allObjects.stl -blockMesh -verbose > log_surfaceCheck
echo "<200> calculate the bounding box"


# <250> write header
python << END
file="system/blockMeshDict"
fl=open(file,"a")
fl.write('FoamFile\n {\n version     2.0;\n format      ascii;\n class       \
dictionary;\n object      blockMeshDict;\n}\n\n')
fl.close()
END
echo "<250> write header in blockMeshDict"

# <350> calculates X,Y,Z
Xmin=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $1}')
#echo $Xmin
Xmax=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $4}')
#echo $Xmax
Ymin=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $2}')
#echo $Ymin
Ymax=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $5}')
#echo $Ymax
Zmin=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $3}')
#echo $Zmin
Zmax=$(cat log_surfaceCheck | grep "Bounding Box :" | tr -d "BoundingBox:()" \
| awk '{print $6}')
#echo $Zmax
echo "<300> calculate current vertices coordinates"


# python section start
# <400> write vertices (-m1: show only the rist result; -A n: show n lines)
python << END
from fractions import gcd
import math
import os
import fileinput
file="system/blockMeshDict"
scaleFactorCubes=float("$1")
dx=round(max($Xmax,$Xmin)-min($Xmax,$Xmin),1)
dy=round(max($Ymax,$Ymin)-min($Ymax,$Ymin),1)
dz=round(max($Zmax,$Zmin)-min($Zmax,$Zmin),1)

print "dX: {0:.5f}".format(dx)
print "dY: {0:.5f}".format(dy)
print "dZ: {0:.5f}".format(dz)

ggt=lambda ns: reduce(lambda x,y:gcd(x,y),ns)
kgv=lambda ns: reduce(lambda x,y:x*y/gcd(x,y),ns)
ggtCalc=ggt([dx,dy,dz])

gradingA = round(dx*scaleFactorCubes,0)*2
gradingB = round(dy*scaleFactorCubes,0)*2
gradingC = round(dz*scaleFactorCubes,0)*2

print(gradingA,gradingB,gradingC)

fl=open(file,"a")
fl.write('vertices\n')
fl.write('(\n    (')

#first line of the vertices list
fl.write(str(int($Xmin-(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymin-(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmin-(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#second line of the vertices list
fl.write('    (')
fl.write(str(int($Xmax+(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymin-(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmin-(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#third line of the vertices list
fl.write('    (')
fl.write(str(int($Xmax+(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymax+(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmin-(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#fourth line of the vertices list
fl.write('    (')
fl.write(str(int($Xmin-(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymax+(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmin-(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#fifth line of the vertices list
fl.write('    (')
fl.write(str(int($Xmin-(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymin-(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmax+(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#sixth line of the vertices list
fl.write('    (')
fl.write(str(int($Xmax+(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymin-(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmax+(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#seventh line of the vertices list
fl.write('    (')
fl.write(str(int($Xmax+(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymax+(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmax+(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n')

#eighth line of the vertices list
fl.write('    (')
fl.write(str(int($Xmin-(($Xmax-$Xmin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Ymax+(($Ymax-$Ymin)*($scaleFactorMesh-1))/2)))
fl.write(' ')
fl.write(str(int($Zmax+(($Zmax-$Zmin)*($scaleFactorMesh-1))/2)))
fl.write(')\n);\n')

fl.write('\nconvertToMeters 1;\n\n')
fl.write('blocks ( hex (0 1 2 3 4 5 6 7) ('+str(int(gradingA))+' '+ \
str(int(gradingB))+' '+str(int(gradingC))+') simpleGrading (1 1 1));\n\n')
fl.write('edges ();\n\n')
fl.write('patches ();\n\n')
fl.write('mergePatchPairs ();')
fl.close()
print "<400> write vertices coordinates in blockMeshDict"

# <450> create blockmesh
os.system("blockMesh > log_blockMesh.txt")
print "<450> create the blockmesh"

# <500> convert unscaled blockMesh to VTK
#os.system("foamToVTK -constant -ascii -useTimeName > log_vtk_convertion1")
#os.system("mv VTK VTK-0")
#print "<500>"
END
# python section end

# <600> convert scaled blockMesh to VTK
foamToVTK -constant -ascii -useTimeName -noFunctionObjects > log_vtk_convertion1.txt
mv VTK VTK_blockMesh
echo "<600>"

# <650> check min/max edge length
checkMesh -allGeometry -allTopology | grep "cells:\|Min/max edge"
echo "<650> check min/max edge lenght and cells amount"

rm -f *.obj

#cp constant/polyMesh/boundary constant/polyMesh/Archive/
#cp constant/polyMesh/faces constant/polyMesh/Archive/
#cp constant/polyMesh/neighbour constant/polyMesh/Archive/
#cp constant/polyMesh/owner constant/polyMesh/Archive/
#cp constant/polyMesh/points constant/polyMesh/Archive/
    
echo "New blockMesh has been created !"
echo "(1) open and check the created mesh in paraFoam"
echo "(2) check the scaled blockMesh stored in the VTK-1 folder. \
#This one will be used for the snappyHexMesh"

