#!/bin/bash
#------------------------------------------------------------------------------
# Script-Name:  easyClean.sh
# Start:	...
# Project:      ...
# Description:  
# Version:      0.016
# Author:       Damian Berghof, virengos.com
# Email:
# Licence:      This program is free software
# Date:         2019-10-17
# Python V.:    -
# Tested on:    -
# Remarks:
#------------------------------------------------------------------------------

read -n 1 -p "*** This script will clean-up the simulation folder! ***"

killall simpleFoam
killall pimpleFoam

clear

rm -f .foamEndJob*
rm -f scsub.term*
#rm -rf postProcessing*

rm -f simpleFoam*
rm -f potentialFoam*
rm -f pimpleDyMFoam*

rm -f terminate.info.*
rm -f job-protokoll-aufrufe*
rm -f snappyHexMesh.*
rm -f snappyHexMesh-*
rm -f machines
rm -rf processor*

# find all numerical folders, remove all but the last one
dirname=""
if [ ! -d ./*last_time_step* ] ; then
    dirname=$(ls -d */ | sort -n | tail -n1)
    echo "*** last result folder will be renamed and kept ***"
    echo "*** folder renamed to: ""${dirname: : -1}"_last_time_step
    mv $dirname "${dirname: : -1}"_last_time_step
fi

echo "*** all other time steps have been deleted! ***"
find . -mindepth 1 -maxdepth 1 -type d -name "*[0-9]" -exec rm -fr {} \;

rm -f badFaces

rm -rf VTK*
rm -f log*
rm -f Log*
rm -f *.obj
rm -f illegalFaces
rm -f problemFaces

rm -f boundary
rm -f cellZones
rm -f faces
rm -f faceZones
rm -f neighbour
rm -f owner
rm -f points
rm -f pointZones

rm -f constant/triSurface/*.eMesh
rm -f constant/extendedFeatureEdgeMesh/*

echo "*** Available OpenFOAM environment variables ***"
env | grep ^FOAM

echo "*** clean-up the postProcessing folder manually ***"
