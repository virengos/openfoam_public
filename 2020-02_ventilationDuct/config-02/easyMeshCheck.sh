#!/bin/bash 

clear

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# 7 - mesh check 
checkMesh -allGeometry -allTopology -constant -writeSets vtk > log_meshCheck.txt
