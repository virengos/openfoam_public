#!/bin/bash
# title         :easySimulation.sh
# description   :
# author        :Damian Berghof
# date          :2019-10-17
# version       :0.05
# usage         :./easySimulation.sh
# notes         :...
#==============================================================================

#clear

cd ${0%/*} || exit 1    # Run from this directory

'''
# check if a last time step exists and rename it back to full numerical folder name
if [ -d ./*last* ] ; then
    dirname=$(ls -d */ | sort -n | tail -n1)
    echo "*** last result folder will be restored ***"
    echo "*** folder renamed to: ""${dirname//[!0-9]/}"_last_time_step
    mv $dirname "${dirname//[!0-9]/}"
fi

echo "*** check in the controldict if simulation starts from <latestTime> ***"
read -n 1 -p "*** and press any key to continue or cancel with ctrl-c ***"
'''

# Source tutorial run0 functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Get application directory
application=$(getApplication)

# reconstruct 0 time folder
rm -rf -v 0
cp -rf -v zero.orig/ 0

numCPU=4;

decomposePar

mpirun -np $numCPU $application -parallel
#mpirun -np 4 simpleFoam -parallel

exit



zip -r test-case.zip test-case
