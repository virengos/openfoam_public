#!/bin/bash 
#------------------------------------------------------------------------------
# Script-Name:  easySnappyHexMesh
# Start:        ./easySnappyHexMesh.sh
# Project:      ...
# Description:  ..
# Version:      0.01
# Author:       Damian Berghof, virengos.com
# Email: 	-
# Licence:      GNU GPLv3 (based on the OpenFOAM CFD toolbox, www.openfoam.org)
# Subject:  	-
# Date:         2019-11-20
# Python V.:    -
# Tested on:    -
# Remarks:
#------------------------------------------------------------------------------


clear

Folder=***${PWD##*/}***
#numCPUs1="4"
#numCPUs2="2"
#mem="2G"
#runTime="1"
#version="4.1"

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions


#echo "*** convert mesh from UNV to OpenFOAM format ***"
#sleep 1
echo "*** surfaceFeatureExtract ***"
sleep 1
echo "*** decomposePar ***"
sleep 1
echo "*** snappyHexMesh ***"
sleep 1

clear


#echo "*** convert mesh from UNV to OpenFOAM format ***"
#ideasUnvToFoam mesh.unv



./easyBlockMesh.sh 0.02   #0.025

echo "*** surfaceFeatureExtract ***"
# 1 - extract edges
surfaceFeatureExtract -dict system/surfaceFeatureExtractDict

# 2 - decompose the mesh for parralel run
echo "*** decomposePar ***"
foamJob decomposePar
sleep 2

echo "*** snappyHexMesh ***"
foamJob -parallel snappyHexMesh -overwrite

touch case.foam

exit

reconstructParMesh -constant

# 4 - scale the mesh from mm -> m
transformPoints -scale '(0.001 0.001 0.001)'

