Last Update: 12-01-2019

__Disclaimer:__ \
This offering is not approved or endorsed by OpenCFD Limited, producer and distributor of the OpenFOAM software via www.openfoam.com, and owner of the OPENFOAM®  and OpenCFD®  trade marks.

----

# Virengos Helper Utilities For OpenFOAM

- [extrudeMesh](#extrudeMesh)
- [easyPostCopyPlanes](#easyPostCopyPlanes)

### extrudeMesh
Simple bash script to automate the mesh extensions creation at the inlet and outlet
* adjust the parameters in the files _system/extrudeMeshDict_inlet_ and _system/extrudeMeshDict_outlet_
* make the script executable by using this command under Linux
* and start it!
```
$ chmod +x easyExtensions.sh
$ easyExtensions.sh
```
* open ParaView and review the modified mesh

![pic1](Images/2019-001-01.png)
Mesh extensions for the inlet and outlet

### easyPostCopyPlanes
This Python script modified and copied created VTK files for planes from all the single _postProcessing_ folders to one folder and added an index to each file. Now, one can easily open and visualize a series of VTK files with the ParaView visualization software.
* copy all the files to your simulation folder
* create or modify the _system/cuttingPlane_ file
  * define the plane parameters and fields (e.g. 'U' | 'p')
* modify the _config.py_ file, by adding the folder and the variables
* start the script with a Python3 interpreter
```
$ python3.6 easyPostCopyPlanes.py
```

![anim](Images/anim-001.gif)
Lightweight Data Visualization

### easyPostRunner
[easyPostRunner](https://gitlab.com/virengos/openfoam_public/tree/master/2020-01_easyPostRunner)

----

## About
### Maintainers
* Email - (info - at - virengos - dot - com)
* Website - [virengos.com](https://www.virengos.com)
* Mastodon - virengos
* Youtube - [virengos channel](https://www.youtube.com/channel/UCYwF43s_ksqIIw8dB7O_TFg?view_as=subscriber)
  * ParaView | Basics of Data Visualization | Best Practice
* Facebook Group _ParaView CAE_ - [ParaView CAE](https://www.facebook.com/groups/352772705405413/)
* Facebook Group _Salome CAE_ - [Salome CAE](https://www.facebook.com/groups/625990377880716/)

### Contributors
* -

----

## Recommended Links

* Digital Ocean Droplet - [Digital Ocean Droplet](https://m.do.co/c/4dea95afbe60)
 * That's the place I'm running my scalable OpenFOAM simulations in the cloud.
